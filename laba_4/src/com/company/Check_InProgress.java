package com.company;

import java.util.ArrayList;

public class Check_InProgress extends ACheck {
    //потоковая функция
    @Override
    public void run() {
        while(true) {
            int k = 0;
            synchronized (orders) {
                // просматриваем список заказов
                for (int i = 0; i < orders.size(); i++) {
                    // если заказ ждёт
                    if (orders.get(i).status == status_order.in_progress) {
                        // пометить что он готов
                        orders.get(i).status = status_order.done;
                        k++;
                    }
                }
            }

            // отладочный вывод (для демонстрации)
            synchronized (orders) {
                System.out.println("[Check_InProgress] Served " + k + " waiting orders.");
                System.out.print("W: ");
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.in_progress)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.print("D: ");
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.done)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.println();
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    // потоковая функция
    @Override
    public void start() {
        if(orders == null) throw new IllegalArgumentException("orders");
        super.start();
    }

    public void AssignCollection(ArrayList<Order> orders) {
        if(this.isAlive()) return;
        this.orders = orders;
    }
}
