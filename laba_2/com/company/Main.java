package com.company;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.UUID;

public class Main {


    // args - массив аргументов командной строки
    // String[] args - массив со строками под названием args
    public static void main1(String[] args) {
        // обьявили имя a для Napitok
        Napitok a;
        //equals сравниваем обьект Cofe с аргументом
        if (args[0].equals("Cofe")){
            //создаём новый пустой обьект Cofe для Napitok c именем a
            a = new Cofe();
        }
        else if (args[0].equals("Chai")){
            a = new Chai();
        }
        else {
            System.out.println("нужно выбрать только чай или кофе");
            return;
        }

        int count = 1; // количество вводимых объектов. по умолчанию 1.
        //length - возвращаем текущий размер обьетов, сколько в ней элементов
        if(args.length >= 2) { // если у нас есть второй аргумент
            //Integer.parseInt - преобразовываем количество обьектов в число
            count = Integer.parseInt(args[1]); // делаем количество объектов равным ему
        }

        // count раз выполняем действия:
        for(int i = 0; i < count; i++) {
            a.create(); // заполнить объект случайными данными
            a.update(); // ввести данные с клавиатуры
            a.read(); // напечатать данные об объекте на экран (на консоль)
        }
    }

    public static void main(String[] args) {
        ArrayList<Napitok> a = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Napitok b = new Chai();
            b.create();
            a.add(b);
        }
        for (int i = 0; i < 3; i++){
            Napitok b = new Cofe();
            b.create();
            a.add(b);
        }

        // напечатать все напитки (чтобы знать, из каких uuid выбирать)
        //System.out.println(a);
        for (int i=0; i<a.size();i++){
            //a.get(i).read();
            Napitok b = a.get(i);
            b.read();
        }

        // создать корзину
        ShoppingCart cart = new ShoppingCart();

        // для ввода
        Scanner sc = new Scanner(System.in);

        for(int k = 0; k < 2; k++) {
            // найти напиток с указанным uuid в а
            System.out.println("Введите UUID товара, который хотите купить: ");
            String UUID = sc.nextLine();
            int idx = -1; // если равно -1 то ничего не нашли
            //size - возвращаем текущий размер коллекций, сколько в ней элементов
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i).ID_tovara.toString().equals(UUID)) {
                    idx = i;
                    break;
                }
            }
            if (idx == -1) {
                System.out.println("Извините мы ничего не нашли");
                return;
            }

            // положить в корзину найденный напиток
            cart.add(a.get(idx));
        }

        // спросить ФИО и емаил, создать объект Credentials
        System.out.print("Введите имя: ");
        String FirstName = sc.nextLine();
        System.out.print("Введите фамилию: ");
        String LastName = sc.nextLine();
        System.out.println("Введите отчество: ");
        String SecondName = sc.nextLine();
        System.out.println("Введите емайл: ");
        String E_mail = sc.nextLine();

        Credentials b = new Credentials(1, LastName, FirstName, SecondName, E_mail);

        // используя корзину и Credentials, создать заказ (объект класса Order).
        Order c = new Order(status_order.in_progress, Date.from(Instant.now()), null, cart, b);
        c.print();

    }
}
