package com.company;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Scanner;

public class AOrderManager0010 extends AManageOrder {

    public AOrderManager0010(ArrayList<Order> orders) {
        super(orders);
    }

    @Override
    public void readByID(int id) throws IOException, ClassNotFoundException {
        // десериализация
        try {
            ObjectInputStream w = new ObjectInputStream(
                    new FileInputStream("order_" + id + ".bin"));

            synchronized (ACheck.mutex) {
                int order_id_in_arraylist = -1;
                for (int i = 0; i < r.size(); i++) {
                    if(r.get(i).id == id) order_id_in_arraylist = i;
                }

                Order o = (Order)w.readObject();

                if (order_id_in_arraylist == -1)
                    r.add(o);
                else
                    r.set(order_id_in_arraylist, o);
            }
            w.close();
        }
        catch(IOException e) {
            System.out.println("Всё очень плохо! " + e.toString());
            throw e;
        }
    }

    @Override
    public void saveByID(int id) throws IOException {
        // сериализация
        try {
            FileOutputStream w;
            ObjectOutputStream w1;
            String out;
            synchronized (ACheck.mutex) {
                Order o = null;
                for (int i = 0; i < r.size(); i++) {
                    if(r.get(i).id == id) o = r.get(i);
                }
                if (o == null)
                    throw new IOException("wrong id" + id);
                w = new FileOutputStream("order_" + id + ".bin");
                w1 = new ObjectOutputStream(w);
                w1.writeObject(o);
                w1.close();
                w.close();
            }
        }
        catch(IOException e) {
            System.out.println("Всё плохо! " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void readAll() throws IOException, ClassNotFoundException {
        // десериализация
        try {
            ObjectInputStream w = new ObjectInputStream(
                    new FileInputStream("orders.bin"));

            synchronized (ACheck.mutex) {
                //for (int i = 0; i < r.size(); i++) {
                //    objectOutputStream.writeObject(r.get(i));
                //}

                ArrayList<Order> r2 = (ArrayList<Order>)w.readObject();
                r.clear();
                r.addAll(r2);

            }
            w.close();
        }
        catch(IOException e) {
            System.out.println("Всё очень плохо! " + e.toString());
            throw e;
        }
    }

    @Override
    public void saveAll() throws IOException {
        // сериализация
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream("orders.bin"));

            synchronized (ACheck.mutex) {
                //for (int i = 0; i < r.size(); i++) {
                //    objectOutputStream.writeObject(r.get(i));
                //}
                objectOutputStream.writeObject(r);
            }
            objectOutputStream.close();
        }
        catch(IOException e) {
            System.out.println("Всё очень плохо! " + e.toString());
            throw e;
        }
    }
}

