package com.company;

import java.util.UUID;

public abstract class Napitok implements ICrudAction {
    protected UUID ID_tovara;

    protected String nazvanie;
    protected int czena;
    protected static int sczetchik_tovarov = 0;
    protected String firma_postavshik;
    protected String strana_proizvoditel;

    public Napitok() {
        ID_tovara = UUID.randomUUID();
    }

    public abstract void create();
    public abstract void read();
    public abstract void update();
    public abstract void delete();

}
