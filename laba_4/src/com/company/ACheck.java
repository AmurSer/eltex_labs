package com.company;

import java.util.ArrayList;

// класс хранит в себе ссылку на коллекцию заказов
// Thread - класс потока, Runnable - интерфейс с методом run
public abstract class ACheck extends Thread {
    // ссылка на коллекцию заказов
    protected ArrayList<Order> orders;

    // потоковая функция
    @Override
    abstract public void run();

}
