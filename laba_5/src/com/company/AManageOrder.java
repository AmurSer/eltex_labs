package com.company;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

abstract public class AManageOrder implements IOrder {

    ArrayList<Order> r;

    public AManageOrder(ArrayList<Order> orders) {
        r = orders;
    }

    public abstract void readByID(int id) throws IOException, ClassNotFoundException;
    public abstract void saveByID(int id) throws IOException;

    public abstract void readAll() throws IOException, ClassNotFoundException;
    public abstract void saveAll() throws IOException;
}
