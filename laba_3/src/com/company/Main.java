package com.company;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {


    // args - массив аргументов командной строки
    // String[] args - массив со строками под названием args
    public static void main1(String[] args) {
        // обьявили имя a для Napitok
        Napitok a;
        //equals сравниваем обьект Cofe с аргументом
        if (args[0].equals("Cofe")){
            //создаём новый пустой обьект Cofe для Napitok c именем a
            a = new Cofe();
        }
        else if (args[0].equals("Chai")){
            a = new Chai();
        }
        else {
            System.out.println("нужно выбрать только чай или кофе");
            return;
        }

        int count = 1; // количество вводимых объектов. по умолчанию 1.
        //length - возвращаем текущий размер обьетов, сколько в ней элементов
        if(args.length >= 2) { // если у нас есть второй аргумент
            //Integer.parseInt - преобразовываем количество обьектов в число
            count = Integer.parseInt(args[1]); // делаем количество объектов равным ему
        }

        // count раз выполняем действия:
        for(int i = 0; i < count; i++) {
            a.create(); // заполнить объект случайными данными
            a.update(); // ввести данные с клавиатуры
            a.read(); // напечатать данные об объекте на экран (на консоль)
        }
    }

    public static void main(String[] args) {
        ArrayList<Napitok> a = new ArrayList<>();
        /*for (int i=0;i<3;i++) {
            Chai b = new Chai();
            b.create();
            a.add(b);
        }
        for (int i=0;i<3;i++) {
            Cofe b = new Cofe();
            b.create();
            a.add(b);
        }*/
        Chai b;
        b = new Chai();
        b.nazvanie = "Lipton";
        b.strana_proizvoditel = "Great Britain";
        b.firma_postavshik = "Lipton Inc.";
        b.vid_upakovki = 1;
        b.czena = 100;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Лисма";
        b.strana_proizvoditel = "Thailand";
        b.firma_postavshik = "Lisma Inc.";
        b.vid_upakovki = 0;
        b.czena = 45;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Brooke Bond";
        b.strana_proizvoditel = "Sri-Lanka";
        b.firma_postavshik = "Brooke Bond Inc.";
        b.vid_upakovki = 0;
        b.czena = 70;
        a.add(b);

        Cofe f;
        f = new Cofe();
        f.nazvanie = "Nescafe";
        f.strana_proizvoditel = "Russia";
        f.firma_postavshik = "Nestle";
        f.vid_cofeinyh_zeren = cofe_type.arabica;
        f.czena = 90;
        a.add(f);

        f = new Cofe();
        f.nazvanie = "Jardin";
        f.strana_proizvoditel = "USA";
        f.firma_postavshik = "Jardin Inc.";
        f.vid_cofeinyh_zeren = cofe_type.robusta;
        f.czena = 200;
        a.add(f);

        for (int i=0;i<a.size();i++){
            Napitok n = a.get(i);
            n.read();
            System.out.println();
        }

        //ShoppingCart<Napitok> cart = new ShoppingCart<>();
        ShoppingCart<Cofe> cart_cofe = new ShoppingCart<>();
        ShoppingCart<Chai> cart_chai = new ShoppingCart<>();

        Scanner sc = new Scanner(System.in);

        for (int k=0;k<2;k++){
            //Найти напиток с указанным UUID товара
            System.out.println("Введите UUID товара который хотите купить: ");
            String UUID = sc.nextLine();
            int idx=-1;//если равно -1 то ничего не нашли
            for (int i=0;i<a.size();i++){
                if (a.get(i).ID_tovara.toString().equals(UUID)){
                    idx=i;
                    break;
                }
            }
            if (idx==-1){
                System.out.println("Извините мы ничего не нашли!");
                return;
            }

            Napitok n = a.get(idx);
            if(n instanceof Chai) { // если объект n является объектом класса Chai
                cart_chai.add((Chai)n);
            }
            else if(n instanceof Cofe) {
                cart_cofe.add((Cofe)n);
            }

            //положить в корзину найденный напиток
            //cart.add(a.get(idx));

        }
        //спросить ФИО и емайл, создать обьект Credentials
        System.out.println("Введите имя: ");
        String FirstName = sc.nextLine();
        System.out.println("Введите фамилию: ");
        String LastName = sc.nextLine();
        System.out.println("Введите отчество: ");
        String SecondName = sc.nextLine();
        System.out.println("Введите E_mail: ");
        String E_mail = sc.nextLine();

        Credentials cr = new Credentials(1, LastName,FirstName,SecondName
                ,E_mail);

        //используя корзину и Credentials, создать заказ(обьект класса Order)
        //Order c = new Order(status_order.in_progress, Date.from(Instant.now()),
            //    null, cart, b);
        //c.print();
        Orders<Order> o = new Orders<>();
        o.Make_purchase(cart_cofe, cr);
        o.Make_purchase(cart_chai, cr);
        o.check_orders();
        o.print_orders();
    }
}
