package com.company;

import java.util.ArrayList;

public class Check_Done extends ACheck {
    //потоковая функция
    @Override
    public void run() {
        while(true) {
            int k = 0;
            synchronized (orders) {
                // просматриваем список заказов
                for (int i = 0; i < orders.size(); i++) {
                    // если заказ выполнен
                    if (orders.get(i).status == status_order.done) {
                        // удалить его
                        orders.remove(i);
                        k++;
                        i--;
                    }
                }
            }

            // отладочный вывод (для демонстрации)
            synchronized (orders) {
                System.out.println("[Check_Done] Removed " + k + " done orders.");
                System.out.print("W: "); // W - в ожидании
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.in_progress)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.print("D: "); // D - обработан
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.done)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.println();
            }

            try {
                Thread.sleep(25000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void start() {
        if(orders == null) throw new IllegalArgumentException("orders");
        super.start();
    }

    public void AssignCollection(ArrayList<Order> orders) {
        if(this.isAlive()) return;
        this.orders = orders;
    }
}
