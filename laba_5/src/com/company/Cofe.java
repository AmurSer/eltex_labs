package com.company;

import java.io.Serializable;
import java.util.Scanner;

//enum - перечисление
//enum с именем cofe_type, далее добавлены - arabica, robusta
enum cofe_type {
    arabica,robusta
}
//extends - расширение(наследование от родительского класса Napitok)
public class Cofe extends Napitok implements Serializable {
    protected cofe_type vid_cofeinyh_zeren;

    //super - вызов конструктора родительского класса
    public Cofe(){
        super();
        sczetchik_tovarov++;
    }

    //метод create – создание объекта со случайными значениями
    public void create() {
        nazvanie = "Tovar " + (int)(Math.random() * 255);
        czena = (int)(Math.random() * 255);
        firma_postavshik = "Pyatorochka" + (int)(Math.random() * 255);
        strana_proizvoditel = "Russia" + (int)(Math.random() * 255);
        vid_cofeinyh_zeren = cofe_type.values()[(int)(Math.random() * 1.99)];
    }
    //метод read – вывод данных на экран
    public void read(){
        System.out.println(ID_tovara.toString());
        System.out.println(nazvanie + " (" + firma_postavshik + " из " + strana_proizvoditel + ")");
        System.out.println("Цена = " + czena + " руб., вид зёрен = " + vid_cofeinyh_zeren);
    }
    //метод update – ввод данных с клавиатуры
    public void update(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите название кофе: ");
        nazvanie=sc.nextLine();
        System.out.print("Введите цену: ");
        czena=sc.nextInt(); sc.nextLine();
        System.out.print("Введите фирму поставщика: ");
        firma_postavshik=sc.nextLine();
        System.out.print("Введите страну производитель: ");
        strana_proizvoditel=sc.nextLine();
        System.out.print("Выберите 0 арабика или 1 робуста: ");
        vid_cofeinyh_zeren=cofe_type.values()[sc.nextInt()]; sc.nextLine();
    }
    //метод delete – принудительное зануление данных в объект
    public void delete(){
        sczetchik_tovarov--;
        //ID_tovara=0;
        nazvanie="";
        czena=0;
        firma_postavshik="";
        strana_proizvoditel="";
        vid_cofeinyh_zeren=cofe_type.arabica;
    }
}
