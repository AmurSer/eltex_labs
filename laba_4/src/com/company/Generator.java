package com.company;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;

// класс генератора заказов.
// через указанный интервал создаёт корзину с 1-2 товаром из списка drinks
// оформляем эту корзину на свои данные и создаёт заказ со статусом "в обработке",
// который добавляется в общую для всех коллекцию заказов.
// генератор работает в отдельном потоке.
public class Generator extends Thread {
    ArrayList<Napitok> drinks; // все доступные напитки в магазине
    ArrayList<Order> orders; // список заказов
    int interval_millis = 0; // интервал срабатывания
    Credentials cr = new Credentials("Robotov", "Generator", "Mashinovich", "robot@napitki-shop.com");
    @Override
    public void run() {
        while(true) { // в бесконечном цикле
            // создать корзину
            ShoppingCart<Napitok> cart = new ShoppingCart<>();
            // выбрать число между 1 и 2
            int count = (int)(Math.random() * 2 + 1);
            // сложить в корзину count товаров
            for(int i = 0; i < count; i++) {
                cart.add(drinks.get( (int)(Math.random() * drinks.size()) ));
            }

            Order o;
            // synchronized нужен, чтобы два потока нечаянно не работали с коллекцией одновременно
            synchronized (orders) {
                // создать заказ
                 o = new Order(
                        status_order.in_progress
                        , Date.from(Instant.now())
                        , Date.from(Instant.now().plus(2, ChronoUnit.DAYS))
                        , cart
                        , cr
                );
                 // добавить к остальным заказам
                orders.add(o);
            }

            // отладочный вывод (для демонстрации)
            // обёрнут в synchronized везде, чтобы разные потоки одновременно на консоль не печатали
            synchronized (orders) {
                System.out.println("[Generator] Generated new order with id = " + o.id);
                System.out.print("W: ");//W - в ожидании
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.in_progress)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.print("D: ");//D - обработан
                for (int i = 0; i < orders.size(); i++) {
                    if(orders.get(i).status == status_order.done)
                        System.out.print(orders.get(i).id + " ");
                }
                System.out.println();
                System.out.println();
            }

            // подождать (или усыпить поток) на указанное число миллисекунд
            try {
                Thread.sleep(interval_millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void start() {
        // перед запуском нужно указать, какие напитки у нас есть, куда складывать заказы и с каким интервалом
        if(orders == null) throw new IllegalArgumentException("orders");
        if(drinks == null) throw new IllegalArgumentException("drinks");
        if(interval_millis <= 0) throw new IllegalArgumentException("interval");
        super.start();
    }

    // коллекция куда складывать заказы
    public void AssignCollection(ArrayList<Order> orders) {
        if(this.isAlive()) return;
        this.orders = orders;
    }

    // интервал срабатывания
    public void SetInterval(int millis) {
        if(this.isAlive()) return;
        interval_millis = millis;
    }

    // какие напитки есть
    public void SetAvailableDrinksCollection(ArrayList<Napitok> drinks) {
        if(this.isAlive()) return;
        this.drinks = drinks;
    }
}
