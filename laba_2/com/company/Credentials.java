package com.company;

public class Credentials {
    //переменные экземпляра класса Credentials(пользователи)
    protected int ID;
    protected String LastName;
    protected String FirstName;
    protected String SecondName;
    protected String E_mail;
    //
    public Credentials (int ID, String LastName, String FirstName,
                        String SecondName, String E_mail){
        //this ссылка на переменные экземпляра класса Credentials
        this.ID = ID;//ссылка на переменную ID и присваиваем ей имя ID
        this.LastName = LastName;
        this.FirstName = FirstName;
        this.SecondName = SecondName;
        this.E_mail = E_mail;
    }
    //метод print выводит на консоль
    public void print(){
        System.out.println(ID);
        System.out.println(LastName);
        System.out.println(FirstName);
        System.out.println(SecondName);
        System.out.println(E_mail);
    }
}
