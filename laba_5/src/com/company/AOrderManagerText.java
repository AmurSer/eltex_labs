package com.company;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Scanner;

public class AOrderManagerText extends AManageOrder {

    private static Gson gson = new Gson();

    @SuppressWarnings("WeakerAccess")
    public AOrderManagerText(ArrayList<Order> orders) {
        super(orders);
    }

    @Override
    public void readByID(int id) throws IOException {
        // десериализация
        try {
            String content = new Scanner(new FileReader("order_" + id + ".txt")).useDelimiter("\\Z").next();
            Order o = gson.fromJson(content, Order.class);
            synchronized (ACheck.mutex) {
                //r.set(id, o);
                int order_id_in_arraylist = -1;
                for (int i = 0; i < r.size(); i++) {
                    if(r.get(i).id == id) order_id_in_arraylist = i;
                }
                if (order_id_in_arraylist == -1)
                    r.add(o);
                else
                    r.set(order_id_in_arraylist, o);
            }
        }
        catch(IOException e) {
            System.out.println("Всё плохо! " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void saveByID(int id) throws IOException {
        // сериализация
        try {
            FileWriter w;
            String out;
            synchronized (ACheck.mutex) {
                Order o = null;
                for (int i = 0; i < r.size(); i++) {
                    if(r.get(i).id == id) o = r.get(i);
                }
                if (o == null)
                    throw new IOException("wrong id" + id);
                w = new FileWriter("order_" + id + ".txt");
                out = gson.toJson(o);
            }
            w.write(out);
            w.close();
        }
        catch(IOException e) {
            System.out.println("Всё плохо! " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void readAll() throws IOException {
        // десериализация
        try {
            String content = new Scanner(new FileReader("orders.txt")).useDelimiter("\\Z").next();
            Type collectionType = new TypeToken<ArrayList<Order>>(){}.getType();
            synchronized (ACheck.mutex) {
                r = gson.fromJson(content, collectionType);
            }
        }
        catch(IOException e) {
            System.out.println("Всё плохо! " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void saveAll() throws IOException {
        // сериализация
        try {
            FileWriter w = new FileWriter("orders.txt");
            String out;
            synchronized (ACheck.mutex) {
                out = gson.toJson(r);
            }
            w.write(out);
            w.close();
        }
        catch(IOException e) {
            System.out.println("Всё плохо! " + e.getMessage());
            throw e;
        }
    }

}
