package com.company;

public class Main {

    public static void main(String[] args) {
        Napitok a;
        if (args[0].equals("Cofe")){
            a = new Cofe();
        }
        else if (args[0].equals("Chai")){
            a = new Chai();
        }
        else {
            System.out.println("нужно выбрать только чай или кофе");
            return;
        }

        int count = 1; // количество вводимых объектов. по умолчанию 1.
        if(args.length >= 2) { // если у нас есть второй аргумент
            count = Integer.parseInt(args[1]); // делаем количество объектов равным ему
        }

        // count раз выполняем действия:
        for(int i = 0; i < count; i++) {
            a.create(); // заполнить объект случайными данными
            a.update(); // ввести данные с клавиатуры
            a.read(); // напечатать данные об объекте на экран (на консоль)
        }
    }
}
