package com.company;

import java.util.UUID;

//implements - реализация(будут созданы все методы из ICrudAction)
public abstract class Napitok implements ICrudAction {
    protected UUID ID_tovara;

    protected String nazvanie;
    protected int czena;
    protected static int sczetchik_tovarov = 0;
    protected String firma_postavshik;
    protected String strana_proizvoditel;

    // функция-конструктор
    public Napitok() {
        ID_tovara = UUID.randomUUID();
    }
    // абстрактные методы для класса Napitok
    public abstract void create();
    public abstract void read();
    public abstract void update();
    public abstract void delete();

}
