package com.company;


import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Napitok> a = new ArrayList<>();

        Chai b;
        b = new Chai();
        b.nazvanie = "Lipton";
        b.strana_proizvoditel = "Great Britain";
        b.firma_postavshik = "Lipton Inc.";
        b.vid_upakovki = 1;
        b.czena = 100;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Лисма";
        b.strana_proizvoditel = "Thailand";
        b.firma_postavshik = "Lisma Inc.";
        b.vid_upakovki = 0;
        b.czena = 45;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Brooke Bond";
        b.strana_proizvoditel = "Sri-Lanka";
        b.firma_postavshik = "Brooke Bond Inc.";
        b.vid_upakovki = 0;
        b.czena = 70;
        a.add(b);

        Cofe f;
        f = new Cofe();
        f.nazvanie = "Nescafe";
        f.strana_proizvoditel = "Russia";
        f.firma_postavshik = "Nestle";
        f.vid_cofeinyh_zeren = cofe_type.arabica;
        f.czena = 90;
        a.add(f);

        f = new Cofe();
        f.nazvanie = "Jardin";
        f.strana_proizvoditel = "USA";
        f.firma_postavshik = "Jardin Inc.";
        f.vid_cofeinyh_zeren = cofe_type.robusta;
        f.czena = 200;
        a.add(f);

        for (int i=0;i<a.size();i++){
            Napitok n = a.get(i);
            n.read();
            System.out.println();
        }

        ArrayList<Order> orders = new ArrayList<>();

        Check_InProgress c1 = new Check_InProgress();
        c1.AssignCollection(orders);
        c1.start();

        Check_Done c2 = new Check_Done();
        c2.AssignCollection(orders);
        c2.start();

        Generator g1 = new Generator();
        g1.AssignCollection(orders);
        g1.SetAvailableDrinksCollection(a);
        g1.SetInterval(3500);
        g1.start();

        //try {
        //    Thread.sleep(100);
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        //}

        Generator g2 = new Generator();
        g2.AssignCollection(orders);
        g2.SetAvailableDrinksCollection(a);
        g2.SetInterval(4200);
        g2.start();

        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("Консоль работает в отдельном потоке. Можете в этом убедиться. Введите текст: ");
            String line = sc.nextLine();
            System.out.println("Вы ввели: \"" + line + "\".");
        }
    }
}
