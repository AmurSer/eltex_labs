package com.company;

import java.util.Scanner;


public class Chai extends Napitok {
    protected int vid_upakovki;//тип упаковки

    //вызов конструктора родительского класса
    public Chai(){
        super();
        sczetchik_tovarov++;
    }


    //метод create – создание объекта со случайными значениями
    public void create() {
        nazvanie = "Tovar " + (int)(Math.random() * 255);
        czena = (int)(Math.random() * 255);
        firma_postavshik = "Pyatorochka" + (int)(Math.random() * 255);
        strana_proizvoditel = "Russia" + (int)(Math.random() * 255);
        vid_upakovki = (int)(Math.random() * 255);
    }
    //метод read – вывод данных на экран
    public void read(){
        System.out.println(ID_tovara.toString());
        System.out.println(nazvanie + " (" + firma_postavshik + " из " + strana_proizvoditel + ")");
        System.out.println("Цена = " + czena + " руб., вид упаковки = " + vid_upakovki);
    }
    //метод update – ввод данных с клавиатуры
    public void update(){
        Scanner sc = new Scanner(System.in);
        nazvanie=sc.nextLine();
        czena=sc.nextInt(); sc.nextLine();
        firma_postavshik=sc.nextLine();
        strana_proizvoditel=sc.nextLine();
        vid_upakovki=sc.nextInt(); sc.nextLine();
    }
    //метод delete – принудительное зануление данных в объект
    public void delete(){
        sczetchik_tovarov--;
        nazvanie="";
        czena=0;
        firma_postavshik="";
        strana_proizvoditel="";
        vid_upakovki=0;
    }
}
