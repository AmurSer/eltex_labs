package com.company;

import java.util.Date;

//enum - перечисление
//enum с именем status_order, далее добавлены - в ожидании и обработан
enum status_order { in_progress, done }

public class Order {
    protected status_order status;//поле статус заказа с именем status
    protected Date created_date, reserved_to;//поле Дата с именами время создания и ожидания
    protected ShoppingCart cart;//поле Корзина с именем корзина
    protected Credentials customer;//поле Пользователи с именем покупатель

    public Order(status_order status, Date created_date, Date reserved_to,
                 ShoppingCart cart, Credentials customer) {
        //this - ссылка
        //ссылка на переменные экземпляров
        this.status = status;//ссылка на переменную экземпляра с именем status и присваиваем имя status
        this.created_date = created_date;
        this.reserved_to = reserved_to;
        this.cart = cart;
        this.customer = customer;
    }
    //метод print выводит на консоль
    public void print() {
        System.out.println(status);
        System.out.println(created_date);
        System.out.println(reserved_to);
        cart.print();
        customer.print();
    }
}

