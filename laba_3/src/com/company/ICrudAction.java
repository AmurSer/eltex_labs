package com.company;

//в interface нельзя создавать объекты
public interface ICrudAction {
    //метод create – создание объекта со случайными значениями
    void create();
    //метод read – вывод данных на экран
    void read();
    //метод update – ввод данных с клавиатуры
    void update();
    //метод delete – принудительное зануление данных в объект
    void delete();
}
