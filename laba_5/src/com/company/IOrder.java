package com.company;

import java.io.IOException;

public interface IOrder {
    public void readByID(int id) throws IOException, ClassNotFoundException;
    public void saveByID(int id) throws IOException;
    public void readAll() throws IOException, ClassNotFoundException;
    public void saveAll() throws IOException;
}
