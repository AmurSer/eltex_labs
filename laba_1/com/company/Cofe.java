package com.company;

import java.util.Scanner;
import java.util.UUID;

enum cofe_type {
    arabica,robusta
}

public class Cofe extends Napitok {
    protected cofe_type vid_cofeinyh_zeren;

    public Cofe(){
        super();
    }

    public void create() {
        sczetchik_tovarov++;
        nazvanie = "Tovar " + (int)(Math.random() * 255);
        czena = (int)(Math.random() * 255);
        firma_postavshik = "Pyatorochka" + (int)(Math.random() * 255);
        strana_proizvoditel = "Russia" + (int)(Math.random() * 255);
        vid_cofeinyh_zeren = cofe_type.values()[(int)(Math.random() * 1.99)];
    }
    public void read(){
        System.out.println(ID_tovara.toString());
        System.out.println(nazvanie);
        System.out.println(czena);
        System.out.println(firma_postavshik);
        System.out.println(strana_proizvoditel);
        System.out.println(vid_cofeinyh_zeren);
    }
    public void update(){
        Scanner sc = new Scanner(System.in);
        //System.out.print("Введите UUID: ");
        //UUID=sc.nextInt(); sc.nextLine();
        System.out.print("Введите название кофе: ");
        nazvanie=sc.nextLine();
        System.out.print("Введите цену: ");
        czena=sc.nextInt(); sc.nextLine();
        System.out.print("Введите фирму поставщика: ");
        firma_postavshik=sc.nextLine();
        System.out.print("Введите страну производитель: ");
        strana_proizvoditel=sc.nextLine();
        System.out.print("Выберите 0 арабика или 1 робуста: ");
        vid_cofeinyh_zeren=cofe_type.values()[sc.nextInt()]; sc.nextLine();
    }
    public void delete(){
        sczetchik_tovarov--;
        //ID_tovara.;
        nazvanie="";
        czena=0;
        firma_postavshik="";
        strana_proizvoditel="";
        vid_cofeinyh_zeren=cofe_type.arabica;
    }
}
