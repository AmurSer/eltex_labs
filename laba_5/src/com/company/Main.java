package com.company;


import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Napitok> a = new ArrayList<>();

        Chai b;
        b = new Chai();
        b.nazvanie = "Lipton";
        b.strana_proizvoditel = "Great Britain";
        b.firma_postavshik = "Lipton Inc.";
        b.vid_upakovki = 1;
        b.czena = 100;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Лисма";
        b.strana_proizvoditel = "Thailand";
        b.firma_postavshik = "Lisma Inc.";
        b.vid_upakovki = 0;
        b.czena = 45;
        a.add(b);

        b = new Chai();
        b.nazvanie = "Brooke Bond";
        b.strana_proizvoditel = "Sri-Lanka";
        b.firma_postavshik = "Brooke Bond Inc.";
        b.vid_upakovki = 0;
        b.czena = 70;
        a.add(b);

        Cofe f;
        f = new Cofe();
        f.nazvanie = "Nescafe";
        f.strana_proizvoditel = "Russia";
        f.firma_postavshik = "Nestle";
        f.vid_cofeinyh_zeren = cofe_type.arabica;
        f.czena = 90;
        a.add(f);

        f = new Cofe();
        f.nazvanie = "Jardin";
        f.strana_proizvoditel = "USA";
        f.firma_postavshik = "Jardin Inc.";
        f.vid_cofeinyh_zeren = cofe_type.robusta;
        f.czena = 200;
        a.add(f);

        for (int i=0;i<a.size();i++){
            Napitok n = a.get(i);
            n.read();
            System.out.println();
        }
        //создать корзину ArrayList c именем orders и присвоить новую пустую корзину
        ArrayList<Order> orders = new ArrayList<>();

        AOrderManagerText m_text = new AOrderManagerText(orders);
        AOrderManager0010 m_0010 = new AOrderManager0010(orders);

        System.out.println("Доступные команды:");
        System.out.println("pause - приостановить, save - сохранить, read - загрузить.");

        //создать класс Check_InProgress обьявим имя c1 и присвоим новый пустой класс Check_InProgress
        Check_InProgress c1 = new Check_InProgress();
        c1.AssignCollection(orders);
        c1.start();

        Check_Done c2 = new Check_Done();
        c2.AssignCollection(orders);
        c2.start();

        Generator g1 = new Generator();
        g1.AssignCollection(orders);
        g1.SetAvailableDrinksCollection(a);
        g1.SetInterval(3500);//3.5 секунды
        g1.start();

        //try {
        //    Thread.sleep(100);
        //} catch (InterruptedException e) {
        //    e.printStackTrace();
        //}

        Generator g2 = new Generator();
        g2.AssignCollection(orders);
        g2.SetAvailableDrinksCollection(a);
        g2.SetInterval(4200);//4.2 секунды
        g2.start();

        Scanner sc = new Scanner(System.in);
        while(true) {
            String line = sc.nextLine();

            synchronized (ACheck.mutex) {
                try {
                    if(line.equalsIgnoreCase("pause")) {
                        ACheck.SetPaused(true);
                    }
                    else if(line.equalsIgnoreCase("unpause")) {
                        ACheck.SetPaused(false);
                    }
                    else if(line.equalsIgnoreCase("status")) {
                        System.out.print("W: ");//W - в ожидании
                        for (int i = 0; i < orders.size(); i++) {
                            if (orders.get(i).status == status_order.in_progress)
                                System.out.print(orders.get(i).id + " ");
                        }
                        System.out.println();
                        System.out.print("D: ");//D - обработан
                        for (int i = 0; i < orders.size(); i++) {
                            if (orders.get(i).status == status_order.done)
                                System.out.print(orders.get(i).id + " ");
                        }
                        System.out.println();
                        System.out.println();
                    }

                    if(line.substring(0, 4).equalsIgnoreCase("save")) {
                        int id = -1;
                        try {
                            id = Integer.parseInt(line.substring(5));
                        }
                        catch(Exception e) {

                        }
                        if(id == -1) {
                            m_0010.saveAll();
                            m_text.saveAll();
                            System.out.println("[i] Сохранение списка успешно завершено");
                        }
                        else {
                            m_0010.saveByID(id);
                            m_text.saveByID(id);
                            System.out.println("[i] Сохранение заказа " + id + " успешно завершено");
                        }
                    }
                    
                    else if(line.substring(0, 4).equalsIgnoreCase("read")) {
                        int id = -1;
                        try {
                            id = Integer.parseInt(line.substring(5));
                        }
                        catch(Exception e) {

                        }
                        if(id == -1) {
                            m_0010.readAll();
                            m_text.readAll();
                            System.out.println("[i] Загрузка списка успешно завершена");
                        }
                        else {
                            m_0010.readByID(id);
                            m_text.readByID(id);
                            System.out.println("[i] Загрузка заказа " + id + " успешно завершена");
                        }
                    }
                }
                catch(Exception e) {
                    System.out.println("[!] " + e.toString());
                    StackTraceElement se[] = e.getStackTrace();
                    for(int i = 0; i < se.length; i++) {
                        System.out.println(se[i].toString());
                    }
                }
            }
        }
    }
}
